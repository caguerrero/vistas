import React from "react";
import "./vistaProveedor.css";

export default function App() {
  return (
    <div className="App">
      <p>
        {" "}
        <strong>Ser un proveedor</strong> es Lorem ipsum dolor sit amet,
        consectetur adipiscing elit. Vestibulum auctor imperdiet quam a dictum.
        Pellentesque finibus lorem nisl, tincidunt ullamcorper dolor accumsan
        sed.{" "}
      </p>
      <img
        alt="sitting"
        src="https://gitlab.com/caguerrero/vistas/-/raw/master/VistaSerUnProveedor/sit_woman.png"
      />
      <form>
        <div id="nombre">
          <label htmlFor="Nombres">
            <strong>Nombres*</strong> <br></br>
          </label>
          <input type="text" placeholder="Nombres" id="Nombres" />
        </div>
        <div id="apellido">
          <label htmlFor="Apellido">
            <strong>Apellido</strong> <br></br>
          </label>
          <input type="text" placeholder="Apellido" id="Apellido" />
        </div>
        <div id="nComp">
          <label htmlFor="NComp">
            <strong>Nombre de la compañia*</strong> <br></br>
          </label>
          <input
            type="text"
            placeholder="(Este sera el nombre con el que publicaras)"
            id="NComp"
          />
        </div>
        <div id="rut">
          <label htmlFor="RUT">
            <strong>RUT*</strong> <br></br>
          </label>
          <input type="text" placeholder="RUT" id="RUT" />
        </div>
        <div id="correo">
          <label htmlFor="Correo">
            <strong>Correo*</strong> <br></br>
          </label>
          <input type="text" placeholder="Correo" id="Correo" />
        </div>
        <div id="telefono">
          <label htmlFor="Telefono">
            <strong>Telefono</strong> <br></br>
          </label>
          <input
            className="telf"
            type="text"
            placeholder="Telefono"
            id="Telefono"
          />
        </div>
        <div id="telefono">
          <label htmlFor="Celular">
            <strong>Celular*</strong> <br></br>
          </label>
          <input
            className="cel"
            type="text"
            placeholder="Celular"
            id="Celular"
          />
        </div>
        <div id="contrasena">
          <label htmlFor="Constrasena">
            <strong>Contraseña*</strong> <br></br>
          </label>
          <input type="text" placeholder="Contraseña" id="Constrasena" />
        </div>
        <div id="ccontrasena">
          <label htmlFor="Ccontrasena">
            <strong>Confirmar Contraseña*</strong> <br></br>
          </label>
          <input
            type="text"
            placeholder="Confirmar Contraseña"
            id="Ccontrasena"
          />
        </div>
        <button>
          <strong>ACEPTAR</strong>
        </button>
        <label id="obligatorio">*Campo obligatorio</label>
        <div id="setCheckbox">
          <input type="checkbox" className="cBox " id="c1" />
          <label id="chbox1" className="chbox" htmlFor="cBox1">
            <span>Lorem ipsum dolor sit amet</span>
          </label>
          <br></br>
          <input type="checkbox" className="cBox" id="c2" />
          <label id="chbox2" className="chbox" htmlFor="cBox2">
            <span>Lorem ipsum dolor sit amet</span>
          </label>
        </div>
      </form>
    </div>
  );
}
