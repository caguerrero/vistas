import React from "react";
import "./vistaSolicitudes.css";

export default function App() {
  return (
    <div className="App">
      <p>
        {" "}
        <strong>Dejate de molestias con proveedores incumplidos</strong> es
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed diam
        nonummy nibh{" "}
        <strong id="resalto">
          <em>
            HAZ TUS SOLICITUDES POR MEDIO DE CLO-APP Y DEJA ESO ATRAS.
            REGISTRATE AQUI.
          </em>
        </strong>{" "}
      </p>
      <img
        src="https://gitlab.com/caguerrero/vistas/-/raw/master/VistaSolicitudes/desk_woman.jpg"
        alt="desk"
      />
      <form>
        <div id="nombre">
          <label htmlFor="Nombres">
            Nombres*<br></br>
          </label>
          <input type="text" placeholder="Nombres" id="Nombres" />
        </div>
        <div id="nComp">
          <label htmlFor="NComp">
            Nombre de la compañia*<br></br>
          </label>
          <input
            type="text"
            placeholder="(Este sera el nombre con el que publicaras)"
            id="NComp"
          />
        </div>
        <div id="telefono">
          <label id="tele" htmlFor="Telefono">
            Telefono <br></br>
          </label>
          <input
            className="telf"
            type="text"
            placeholder="Telefono"
            id="Telefono"
          />
        </div>
        <div id="celular">
          <label htmlFor="Celular">
            Celular*<br></br>
          </label>
          <input
            className="cel"
            type="text"
            placeholder="Celular"
            id="Celular"
          />
        </div>
        <div id="correo">
          <label htmlFor="Correo">
            Correo* <br></br>
          </label>
          <input type="text" placeholder="Correo" id="Correo" />
        </div>
        <div id="contrasena">
          <label htmlFor="Constrasena">
            Contraseña* <br></br>
          </label>
          <input type="text" placeholder="Contraseña" id="Constrasena" />
        </div>
        <div id="ccontrasena">
          <label htmlFor="Ccontrasena">
            Confirmar Contraseña*<br></br>
          </label>
          <input
            type="text"
            placeholder="Confirmar Contraseña"
            id="Ccontrasena"
          />
        </div>
        <button>
          <strong>ACEPTAR</strong>
        </button>
        <label id="obligatorio">*Campo obligatorio</label>
        <div id="setCheckbox">
          <input type="checkbox" className="cBox " id="c1" />
          <label id="chbox1" className="chbox" htmlFor="cBox1">
            <span>
              Acepta nuestra política de datos.{" "}
              <a
                href="https://google.com.co"
                target="_blank"
                rel="noopener noreferrer"
              >
                LEELA AQUÍ
              </a>
            </span>
          </label>
          <br></br>
          <input type="checkbox" className="cBox" id="c2" />
          <label id="chbox2" className="chbox" htmlFor="cBox2">
            <span>Recibir correos informativos de CLO </span>
          </label>
        </div>
      </form>
    </div>
  );
}
