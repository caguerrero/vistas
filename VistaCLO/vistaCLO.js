import React from "react";
import "./vistaCLO.css";

export default function App() {
  return (
    <div className="App">
      <center>
        <img
          width="150"
          src="https://gitlab.com/caguerrero/vistas/-/raw/master/VistaCLO/CLOlogo.png"
          alt="CLO"
        />
      </center>
      <p>
        <strong>Hola Once Publicidad!</strong>, estas a pocos pasos de ser uno
        de nuestros proveedores
      </p>
      <img
        id="box"
        src="https://gitlab.com/caguerrero/vistas/-/raw/master/VistaCLO/girl-box.png"
        alt="box"
      />
      <form>
        <div className="variosCheck">
          <label htmlFor="variosCheck">Experiencia en el sector</label>
          <br></br>
          <input type="checkbox" id="chexp1" id="p1" />
          <label htmlFor="p1">1 - 2 años</label>
          <input type="checkbox" className="chexp" id="p2" />
          <label htmlFor="p2">3 - 5 años</label>
          <input type="checkbox" className="chexp" id="p3" />
          <label htmlFor="p3">+5 años</label>
        </div>
        <div id="products">
          <label htmlFor="productos">Tipos de productos que realizas</label>
          <br></br>
          <input
            type="text"
            placeholder="Ejemplo: Cajas, esferos, bolsas plásticas, acrílico, estampación, etc."
            id="productos"
          />
        </div>
        <div id="materials">
          <label htmlFor="materiales">Tipos de materiales que manejas</label>
          <br></br>
          <input
            type="text"
            placeholder="Ejemplo: Carton ecológico, papel fino, papel craf importado, estampación, etc."
            id="materiales"
          />
        </div>
        <div id="services">
          <label htmlFor="servicios">Servicios que prestas</label>
          <br></br>
          <input
            type="text"
            placeholder="Ejemplo: Instalación de acrílico, corte láser,troquelado, etc."
            id="servicios"
          />
        </div>
        <div className="checkmateriales">
          <label htmlFor="variosCheck">
            ¿Trabajas con materiales ecológicos y/o biodegradables?
          </label>
          <br></br>
          <input id="chmaterial1" type="checkbox" />
          <label htmlFor="p1">Si</label>
          <input className="chmaterial" type="checkbox" />
          <label htmlFor="p2">No</label>
        </div>
        <div className="checkProduccion">
          <label htmlFor="variosCheck">
            Minima cantidad de producción (unidades)
          </label>
          <br></br>
          <input id="chprod1" type="checkbox" />
          <label htmlFor="p1">1 - 20</label>
          <input className="chprod" type="checkbox" />
          <label htmlFor="p2">20 - 50</label>
          <input className="chprod" type="checkbox" />
          <label htmlFor="p3">50 - 100</label>
        </div>
        <div id="tArea">
          <label htmlFor="area">Danos una breve descripción de ti</label>
          <br></br>
          <textarea
            id="area"
            cols="60"
            rows="15"
            maxLength="500"
            placeholder="En ONCE PUBLICIDAD llevamos mas de 15 años en el mercado realizando diferentes tipos de producciones como..."
          />
        </div>
        <div id="archivos">
          <div id="ar1">
            <label>Adjuntar copia RUT</label>
            <br></br>
            <span className="ladjuntar">
              <label className="lab" htmlFor="file">
                Adjuntar archivo
              </label>
            </span>
            <br></br>
            <input
              id="file"
              className="inputfile"
              type="file"
              name="file"
              accept=".pdf,.jpg,.png"
            />
          </div>
          <div id="ar2">
            <label>Adjuntar copia Cédula</label>
            <br></br>
            <span className="ladjuntar">
              <label className="lab" htmlFor="file1">
                Adjuntar archivo
              </label>
            </span>
            <br></br>
            <input
              id="file1"
              className="inputfile"
              type="file"
              name="file"
              accept=".pdf,.jpg,.png"
            />
          </div>
        </div>
        <button>
          <strong>ACEPTAR Y ENVIAR</strong>
        </button>
      </form>
    </div>
  );
}